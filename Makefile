all: install

build:
	rm -rf ../output
	ikiwiki --verbose --setup "`pwd`/conf/help.torproject.org.setup"
	#rsync -a additional/. ../output/.
	cd ../output && rm -rf ikiwiki recentchanges sandbox shortcuts templates wikiicons favicon.ico

install: build
	! [ $$(hostname) = "staticiforme" ] || sudo -u mirroradm static-master-update-component help.torproject.org

.PHONY: build install
